#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ad9850 import AD9850
import Adafruit_BBIO.GPIO as GPIO
from Adafruit_BBIO.SPI import SPI
import time
import argparse


default_reference = 125000000


class spi_decorator:
    def __init__(self, adafruit_spi):
        self.spi = adafruit_spi
        self.MOTOROLA = None
        self.msbFirst = True

    def set_config(self, cfg):
        """'frequency': freq,
        'dataWidth': 8,
        'protocol': spi.MOTOROLA,
        'isMsbFirst': True,
        'isMaster': True,
        'isContinuousMode': True,
        'isCpha': False,
        'isCpol': False,
        """
        self.spi.mode = ((1 << 1) if cfg['isCpol'] else 0) + (1 if cfg['isCpha'] else 0)
        print('SPI_MOde CPOL={}, CPHA={}'.format(cfg['isCpol'], cfg['isCpha']))
        self.msbFirst = not cfg['isMsbFirst']
        self.spi.msh = cfg['frequency']
        self.spi.bpw = cfg['dataWidth']
        self.spi.cshigh = False
        return 0

    def endianConvert(self, data):
        # error selecting endian in hardware, it allways LSB first
        if self.msbFirst:
            res = []
            for b in data:
                r = 0
                for i in range(8):
                    if b & (1 << i):
                        r |= 1 << (7 - i)
                res.append(r)
            return res
        return data

    def send(self, buf):
        ord2 = []
        for letter in buf:
            ord2.append(ord(letter))
        d = self.endianConvert(ord2)
        self.spi.writebytes(d)


class gpio_decorator:
    def __init__(self, pin):
        self.pin = pin
        GPIO.setup(self.pin, GPIO.OUT)

    def set(self, val):
        v = GPIO.HIGH if val else GPIO.LOW
        GPIO.output(self.pin, v)

    def get(self, pin):
        return 0


def freq2Code(f, k):
    return int(f * k)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--start', type=float, default=100, help='min Frequency')
    parser.add_argument('-M', '--stop', type=float, default=100000, help='max Frequency')
    parser.add_argument('-S', '--step', type=float, default=0.1, help='Change Step')
    parser.add_argument('-T', '--updateInterval', type=float, default=1, help='Hold interval')
    parser.add_argument('-B', '--referenceFreq', type=int, default=default_reference,
                        help='Reference frequency (default: {} Hz)'.format(default_reference))

    args = parser.parse_args()

    r = SPI(0, 0)
    spi = spi_decorator(r)
    reset_pin = gpio_decorator('P9_23')
    dds = AD9850(spi, RESET=reset_pin)
    dds.reset()

    inc = 1.0
    F = 0
    k = pow(2, 32) / float(args.referenceFreq)
    print('k={}'.format(k))
    print('step = {}, every {} s.'.format(args.step, args.updateInterval))

    while True:
        F += inc * args.step
        if F >= args.stop:
            inc = -1.0
            F = args.stop
        elif F <= args.start:
            inc = 1.0
            F = args.start

        c = freq2Code(F, k)
        print('F = {} ({})'.format(F, c))
        dds.send(c)

        time.sleep(args.updateInterval)


if __name__ == '__main__':
    main()
